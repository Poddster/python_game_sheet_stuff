import csv
import re


COL_NAME = 0
COL_SCORE = 1
COL_PLUS = 2
COL_EQ = 3
COL_NEG = 4
COL_CMP_COUNT = 5
COL_PLAYED = 6
COL_DUPE = 7
COL_DATE = 8
COL_PLATFORM = 9
COL_NOTES = 10
COL_SERIES = 11
COL_DEV = 12
COL_PUB = 13
COL_MY = 14
COL_THEME = 15
COL_SUBTHEME = 16
COL_COO = 17

wiki_remake_info = [
    "compl", "port", "rerel", "remake",
    "remake or port", "port or remake", "limit",
    "open source", "port re-release",
    "limit, compl", "overclocked", "windows re-release",
    "Port, Remake", "Orig",
]
wiki_cc = [
    "EN", "JP", "NA", "US", "EU", "RU", "JP/NA", "NA/EU",
    "DE", "NA/PAL", "NA/UK", "NA/AU", "UK", "JP/UK",
    "U.S./U.K.",
]
wiki_rubbish_comments = {
    "FS2 SCP was also ported to Linux and Mac OS X": ", Linux, Mac OS X",
    "Atari 400/800/XL/XE": "ATR",
    "Commodore 16/116/Plus/4": "C16",
    "-Present":"",
    "– Collector's Edition": "",
    "- Game of the Year Edition": "",
    "– Game of the Year Edition": "", #????????
}

re_wiki_crap = [
    re.compile(r"\[[0-9]{0,3}\]"),
    re.compile(r"\[.*citation\s+needed.*]"),
    re.compile(r"\((" + str.join("|", wiki_cc) + ")\)"),
    re.compile(r"\((" + str.join("|", wiki_remake_info) + ")\)", re.IGNORECASE)
]


def remove_wiki_crap(wiki_str:str):
    for regexp in re_wiki_crap:
        match = True
        while match:
            match = regexp.search(wiki_str)
            if match:
                # print("\twikicrap:", wiki_str, "matches!!!", regexp)
                wiki_str = wiki_str[:match.start()] + wiki_str[match.end():]
    wiki_str = wiki_str.replace("  ", " ")
    wiki_str = wiki_str.replace("–", "-") # unicode sucks

    for (comment, replacement) in wiki_rubbish_comments.items():
        wiki_str = wiki_str.replace(comment, replacement)
    return wiki_str.strip()


# #
# condition strings into a normalised form
#
def fix_name(name: str):
    # fix issues with "XYZ,The" and "The XYZ"
    if name.endswith(', The') or name.endswith(', the'):
        return "The " + name[:-5]
    else:
        return name


def choose_smallest_date(date_split):
    if len(date_split) > 1 and all(map(str.isdigit, date_split)):
        dates = date_split
        dates_i = [int(d) for d in dates]
        dates_i = sorted(dates_i)
        return str(dates_i[0])
    else:
        return date_split[0]


def fix_date(date):
    # nuke everything?/country? in brackets
    # remove C.
    # take least date?
    date = date.replace("{", "").replace("}", "")
    date = date.replace("?", "")
    date = date.replace("-", " ")
    if not date:
        return ""





    if date.startswith("c. ") or date.startswith("C. "):
        date = date[3:]

    date_split = date.replace(",", "").split()
    return choose_smallest_date(date_split)


plat_translation_table = {
    "AMIGA": "AMI",
    "AMI": "AMI",
    "AMIOS": "AMIGA OS",
    "AMIGA OS": "AMIGA OS",
    "AMI32": "CD32",
    "CD32": "CD32",
    "COMMODORE AMIGA": "AMI",
    "AMIGA CD32": "CD32",
    "CDTV": "AMI",
    "C64": "C64",
    "COMMODORE 64": "C64",
    "PET": "PET",  # commodre pet
    "C128": "C128",
    "COMMODORE 128": "C128",
    "VIC20": "VIC20",
    "C16": "C16",

    # Sony
    "PLAYSTATION": "PS1",
    "PS": "PS1",
    "PLAYSTATION 2": "PS2",
    "PS1": "PS1",
    "PS2": "PS2",
    "PS3": "PS3",
    "PSN": "PSN",
    "PSP": "PSP",
    "VITA": "VITA",
    "PSV": "VITA",


    "PC": "PC",
    "PC?": "PC",
    "DOS": "DOS",  # PC?
    "MS-DOS": "DOS",  # PC?
    "WIN": "WIN",  # PC?
    "WINDOWS": "WIN",
    "WINDOWS MOBILE": "WINMOB",
    "WINMOB": "WINMOB",
    "MICROSOFT WINDOWS": "WIN",
    "WIN3X": "WIN",
    "WINDOWS 3.X": "WIN",
    "WIN9X": "WIN",
    "WIN98": "WIN",
    "WIN95": "WIN",
    "WINNT": "WIN",
    "WINDOWS 7": "WIN",
    "XP": "WIN",
    "WINDOWS 9X": "WIN",

    "XBOX": "XBOX",
    "X360": "X360",
    "360": "X360",
    "XBOX 360 LIVE": "X360",
    "XBLA": "X360",
    "XBLIG": "X360",

    "LIN": "LIN",  # PC?
    "LINUX": "LIN",
    "UNIX": "UNIX",
    "BEOS": "BEOS",  #??
    "OS2": "OS2",
    "TK": "LIN",  #tk windowing
    "X11": "LIN",
    "XO1": "LIN",
    "SOS": "STEAMOS", #?? LIN??
    "STEAMOS": "STEAMOS",

    "MAC": "MAC",  #PC?
    "MAC OS": "MAC",
    "MACINTOSH": "MAC",  #PC?
    "MAC OS X": "MAC",
    "OS X": "MAC",
    "OSX": "MAC",
    "APPLE MAC": "MAC",
    'APPLE II': "APPII",
    "APPLE IIC": "APPII",
    "APPLE II SERIES": "APPII",
    "APPLE II": "APPII",
    "APPLE IIGS": "APPII",
    "APPII": "APPII",
    "APPGS": "APPGS",  #"Apple IIGS"
    "IPOD": "IOS",
    "IPHONE": "IOS",
    "IPAD": "IOS",
    "IOS": "IOS",  #MOBILE?
    #"(IOS)": "",

    #phone/browser etc
    "SYB": "SYMBIAN",  #??? SYB ??
    "EPOC16": "SYMBIAN",
    "EPOC32": "SYMBIAN",
    "SYMBIAN": "SYMBIAN",
    "DROID": "ANDROID",
    "ANDROID": "ANDROID",
    "MOBI": "MOBI",
    "MOBILE PHONES": "MOBI",
    "MOBILE": "MOBI",
    "PHONE": "MOBI",
    "PHONES": "MOBI",
    "ARCADE": "ARCADE",
    "ARC": "ARCADE",
    "CROSS": "CROSS",
    "PBEM": "PBEM",
    "BBS": "BBS",
    "CLOUD": "",  #??
    "JAVA": "JAVA",
    "JAVA ME": "JAVA",
    "JAVAME": "JAVA",
    "JAVASE": "JAVA",
    "J2ME": "JAVA",
    "FLASH": "BROWSER",
    "FLA": "BROWSER",  #???
    #"(FLASH)": "BROWSER",
    "BROW": "BROWSER",
    "BROWSER": "BROWSER",
    "HTML": "BROWSER",
    "HTML5": "BROWSER",
    "CUSTOM PLUGIN": "BROWSER",
    "ONLINE": "BROWSER",
    "WEB": "BROWSER",
    "INTERNET": "BROWSER",
    "EVERYTHING": "",
    "VARIOUS": "",
    "NGE": "NGE",  #nokia n-gage
    "PALM": "PALM",
    "PALM OS": "PALM",


    #8bit home computers
    'AMSTRAD CPC': "CPC",
    "CPC": "CPC",
    "ZX SPECTRUM": "ZX",
    "ZX": "ZX",
    'BBC MICRO': "BBC",
    "BBC": "BBC",
    'ATARI ST': "ST",
    "ST": "ST",  #ATARI ST
    "ATARI 2600": "ATR",
    "ATARI 5200": "ATR",
    "ATR": "ATR",  #8BIT ATARI
    "ATARI": "ATR",
    "ATARI 8-BIT": "ATR",
    "ATARI 8-BIT FAMILY": "ATR",
    "ATR26": "ATR",
    "ATARI LYNX": "LYNX",
    "LYNX": "LYNX",
    "ACRN": "ACRN",
    "ARCHIMEDES": "ACRN",
    'ACORN ARCHIMEDES': "ACRN",
    'ACORN ELECTRON': "ACRN",  #could also be BBC, or its own?
    "PPC": "PPC",
    "RISC OS": "RISC OS",
    "ROS": "RISC OS",

    #sega
    "DREAMCAST": "DC",
    "DC": "DC",
    "SAT": "SAT",
    "SATURN": "SAT",
    "SEGA SATURN": "SAT",
    "SCD": "SCD",
    "SG1K": "SG1K",  #sg-1000
    "GG": "GG",  #gamegear?
    "SEGA CD": "SCD",
    "GEN": "MEGA",
    "GENESIS": "MEGA",
    "SEGA": "MEGA",
    "SEGA MEGA DRIVE": "MEGA",
    "MEGA DRIVE": "MEGA",
    "MEGA DRVE": "MEGA",
    "MEGA": "MEGA",
    "SMD": "MEGA",
    "SMS": "SMS",
    "SEGA MASTER SYSTEM": "SMS",
    "32X": "MEGA",
    "S32X": "MEGA",

    #nintendo
    "FDS": "FDS",  #Famicom Disk System
    "DS": "DS",
    "NINTENDO DS": "DS",
    "DSN": "DS",
    "3DS": "3DS",
    "NINTENDO 3DS": "3DS",
    "GAMEBOY": "GB",
    "GAME BOY": "GB",
    "GB": "GB",
    "GBA": "GBA",
    "GBC": "GBC",
    "GCN": "GCN",#??
    "NGC": "GCN",#??
    "CGN": "GCN",#??
    "GAME CUBE": "GCN",
    "GAMECUBE": "GCN",
    "NINTENDO GAMECUBE": "GCN",
    "NINTENDO 64": "N64",
    "N64": "N64",
    "NES": "NES",
    "SNES": "SNES",
    "SUPER NES": "SNES",
    "WII": "WII",
    "WIIU": "WIIU",
    "WII U": "WII U",
    "VC": "WIIVC",  #Virtual Console (Wii)
    "VIRTUAL CONSOLE": "WIIVC",
    "WIIVC":"WIIVC",
    "NINTENDO VS. ARCADE": "NES",
    "NINTENDO FAMICOM": "NES",
    "FAMICOM": "NES",
    "VS. SYSTEM": "NES",
    "VS. UNISYSTEM": "NES",
    "VS. DUALSYSTEM": "NES",

    "NOOK": "NOOK",
    "KINDLE": "KINDLE",

    #misc
    "ZETA": "ZETA",
    "TRS80": "TRS80",
    "TRS-80": "TRS-80",
    "TRSCC": "TRSCC",  #TRS-80 Color computer
    "DG32": "DG32",  #Dragon 32/64 -- similiar to TRS80
    "CLV": "CLV",  #???
    "WSC": "WSC",  #??? WonderSwan Color???
    "JAGUAR": "JAG",
    "JAG": "JAG",
    "NEWS": "NEWS",
    "FM7": "FM7",  #FM-7
    "FMT": "FMT",  #FM Towns
    "FM TOWNS": "FMT",
    "MSX": "MSX",
    "MSX2": "MSX2",
    "PC60": "PC60",  # NEC PC-6001
    "PC88": "PC88",  # NEC PC-8801
    "PC98": "PC98",  # NEC PC-9801
    "PCJR": "PCJR",  #IBM PCjr"
    "NEC PC-6001": "PC60",
    "NEC PC-8801": "PC88",
    "NEC PC-9801": "PC98",
    "DUO": "DUO",  #TurboDuo
    "PCD": "PCD",  # TurboGrafx-CD / PC Engine CD
    "PCE": "PCE",  # TurboGrafx-16 / PC Engine
    "PCFX": "PCFX",  #PC-FX
    "PLATO": "PLATO",
    "TI99":"CALC",
    "CALC":"CALC",
    "AQUA": "AQUA", #"Mattel Aquarius"
    "ORIC":"ORIC", #ORIC? ORIC TANGERINE? ORIC CPC THOMSON?
    'ORIC CPC THOMSON':"ORIC",
    "X1": "X1",  #SHARP X1
    "SHARP X1": "X1",
    "X68K": "X68K",  #SHARP X68000
    "SHARP X68000": "X68K",
    "NOT": "",  #random entry in Ceasar III
    "BOARDGAME": "BOARDGAME",
    "WS": "WS",  #wonderswan -- I should just delete anything released on WS
    "ODYSSEY": "ODY",
    "ODY": "ODY",
    "MAGNAVOX ODYSSEY": "ODY",
    "ODYSSEY 2": "ODY2",
    "ODY2": "ODY2",
    "3DO": "3DO",
    "CD-I": "CDI",  #phillips CD-i
    "CDI": "CDI",  #phillips CD-i
    "PDP-10": "MAIN",
    "MAIN": "MAIN",
    "MAINFRAME": "MAIN",
    "DESQ": "DESQ",  #"DESQview":
    "DEC": "VAX",
    "VAX": "VAX",
    "T1000": "T1000",  #toshiba 1000
    "INT": "INT",  #Intellivision
    "TANDY": "TANDY",
    "CVIS": "CVIS",
    "COLECOVISION": "CVIS",
    "BARCODE":"BARCODE",
    "BBARII": "BARCODE", #Super Barcode Wars?
    "AND": "",  #random word
    "THE": "",  #random word
    "VERSIONS": "",  #random world
    "2009": "",
    "2012": "",

}


def translate_platform(plat, valid_plats):
    sanitised_value = plat_translation_table[plat.upper()]
    if sanitised_value:
        valid_plats.append(sanitised_value)


def uniq_list_join(joiner, split_plats):
    return str.join(joiner, sorted(set(split_plats)))


def split_strip(splitter:str, str_to_split:str) -> [str]:
    split_str = str_to_split.split(splitter)
    split_str = map(str.strip, split_str)
    split_str = filter(bool, split_str)
    return list(split_str)


def fix_platform(platforms):
    platforms = platforms.replace("(", "").replace(")", "")
    platforms = platforms.replace(";", ",")
    platforms = str.join(",", platforms.split("/"))
    split_plats = split_strip(",", platforms)

    # most of the data is seperated by ",", but some are seperated by ' '. Annoyingly,
    # some are like this 'Microsoft Windows', 'OS X', and some like this: 'DOS AMI'
    # grr
    valid_plats = []
    for plat in split_plats:
        # print(plat)
        if not plat:
            continue
        if plat.upper() in plat_translation_table:
            translate_platform(plat, valid_plats)
        else:
            for sub_plat in plat.split():
                translate_platform(sub_plat, valid_plats)

    valid_plats = filter(bool, valid_plats)

    # uniq and re-merge the list
    return uniq_list_join(", ", valid_plats)


def fix_row(row):
    # remove any wikipedia crap, e.g. square brackets
    new_row = [remove_wiki_crap(s) for s in row]

    new_row[COL_NAME] = fix_name(new_row[COL_NAME])
    new_row[COL_DATE] = fix_date(new_row[COL_DATE])
    new_row[COL_PLATFORM] = fix_platform(new_row[COL_PLATFORM])

    # print("fixed:", new_row)

    return new_row


# #
# Merge
#
def merge_date(last_row, new_row):
    last_date = last_row[COL_DATE]
    new_date = new_row[COL_DATE]
    assert (not last_date or last_date.isdigit()) and (not new_date or new_date.isdigit()), \
        ("dates are not digits?",
         "last:", repr(last_row[COL_NAME]), repr(last_date),
         "new :", repr(new_row[COL_NAME]), repr(new_date))

    if not last_date:
        return new_date
    elif not new_date:
        return last_date

    both = []
    both.extend(last_date.split())
    both.extend(new_date.split())

    return choose_smallest_date(both)


def merge_listy_cell(last_cell, new_cell, splitter, remove_substrings=False):
    split_last_cell = split_strip(splitter, last_cell)
    split_new_cell = split_strip(splitter, new_cell)
    joiner = splitter + " "
    split_cells = split_last_cell + split_new_cell

    if not split_cells:
        return ""

    if not remove_substrings:
        return uniq_list_join(joiner, split_cells)
    else:
        # for each string
        # if the string is a super string of one in the output list, replace it
        # else if it' a substring, ignore it
        #else add it
        uniq_cells = sorted(set(split_cells))
        output = [uniq_cells[0]]
        for a in uniq_cells[1:]:
            output_copy = output[:]
            for (i, b) in enumerate(output_copy):
                assert a != b, (a, b)
                if b in a:
                    output[i] = a
                elif a in b:
                    pass
                else:
                    output.append(a)
        return str.join(joiner, output)


def merge_should_match(last_row, new_row, index):
    last_cell = last_row[index]
    new_cell = new_row[index]
    if not last_cell:
        return new_cell
    elif not new_cell:
        return last_cell
    elif last_cell != new_cell:
        print("err:", last_row)
        print("err:", new_row)
        print("\tRow index {0} should match?! Found {1} and {2}".format(
            index, repr(last_cell), repr(new_cell)
        ))

        return merge_listy_cell(last_cell, new_cell, ",", remove_substrings=True)
    else:
        return last_cell


def merge_rows(sheet, new_row):
    last_row = sheet[-1]

    assert len(last_row) == len(new_row)

    for i in [COL_NAME]:
        assert last_row[i].lower() == new_row[i].lower(), \
            ("Row index {0} should match?! Found {1} and {2}".format(
                i, repr(last_row[i]), repr(new_row[i])
            ))

    for i in [COL_SCORE, COL_PLUS, COL_EQ, COL_NEG, COL_CMP_COUNT,
              COL_PLAYED, COL_MY]:
        new_val = merge_should_match(last_row, new_row, i)
        # print("\tmerge_rows:", i, repr(last_row[i]), repr(new_row[i]), "=>", repr(new_val))
        last_row[i] = new_val

    for i in [COL_DEV, COL_DUPE, COL_PUB, COL_PLATFORM, COL_THEME, COL_SUBTHEME, COL_SERIES, COL_COO]:
        last_row[i] = merge_listy_cell(last_row[i], new_row[i], ",", remove_substrings=True)

    last_row[COL_DATE] = merge_date(last_row, new_row)
    last_row[COL_PLATFORM] = merge_listy_cell(last_row[COL_PLATFORM], new_row[COL_PLATFORM], ",")
    last_row[COL_NOTES] = merge_listy_cell(last_row[COL_NOTES], new_row[COL_NOTES], ".")

    return new_row


def is_duplicate_of_last_row(sheet, new_row):
    if not sheet:
        return

    last_row = sheet[-1]
    if new_row[COL_NAME].lower() == last_row[COL_NAME].lower():
        # print(new_row[COL_NAME], "is a duplicate!")
        return True

    return False


def add_row(sheet, new_row):
    sheet.append(new_row)


def read_sheet(input_sheet_name):
    sheet = []

    import collections

    q = collections.deque(maxlen=4)

    with open(input_sheet_name, newline='', encoding='utf-8') as input_csvfile:
        reader = csv.reader(input_csvfile, delimiter='\t', quotechar='|')

        for (i, row) in enumerate(reader):
            dbg_filter = False
            if dbg_filter:
                interesting_range = [14, 21]
                if 0 < i < interesting_range[0]:
                    continue
                if i > interesting_range[1]:
                    break

            q.append(row)

            if row[COL_DATE] in ["Cancelled", "TBA"]:
                continue

            try:
                # Skip the first row, as it's the column headers rather than actual data
                if i != 0:
                    new_row = fix_row(row)
                else:
                    new_row = row

                if is_duplicate_of_last_row(sheet, new_row):
                    merge_rows(sheet, new_row)
                else:
                    add_row(sheet, new_row)
            except:
                for (j, x) in enumerate(q):
                    z = i - len(q) + j + 1
                    print(z, x)
                raise

    return sheet


def write_sheet(output_file_name, sheet):
    with open(output_file_name, "w", encoding='utf-8', newline='') as output_csvfile:
        writer = csv.writer(output_csvfile, delimiter='\t', quotechar='|')
        for row in sheet:
            writer.writerow(row)


def main():
    import argparse

    parser = argparse.ArgumentParser(description='Filter wikipedia duplicated crap from tsv files')
    parser.add_argument('-i', '--input', metavar='FILENAME', type=str,
                        help='input file', default="test_file.tsv", required=True)
    parser.add_argument('-o', '--output', metavar='FILENAME', type=str,
                        help='output file', default="output.tsv", required=True)
    args = parser.parse_args()
    sheet = read_sheet(args.input)

    top = sheet[0]
    sheet = list(sorted(sheet[1:]))
    sheet.insert(0, top)
    # for x in sheet:
    # print(x)
    write_sheet(args.output, sheet)


if __name__ == '__main__':
    main()
