import csv
import pprint
import sys
import random


COL_NAME = 0
COL_SCORE = 1
COL_PLUS = 2
COL_EQ = 3
COL_NEG = 4
COL_CMP_COUNT = 5
COL_PLAYED = 6
COL_DUPE = 7
COL_DATE = 8
COL_PLATFORM = 9
COL_NOTES = 10
COL_SERIES = 11
COL_DEV = 12
COL_PUB = 13
COL_MY = 14
COL_THEME = 15
COL_SUBTHEME = 16
COL_COO = 17

COLUMN_NAMES = {

}

def make_COLUMN_NAMES():
    global COLUMN_NAMES
    col_filter = filter(lambda x: x[0].startswith("COL_"), globals().items())
    for (k,v) in col_filter:
        k = k[len("COL_"):]
        COLUMN_NAMES[k] = v
make_COLUMN_NAMES()

def reverse_column_name(i):
    for (k,v) in COLUMN_NAMES.items():
        if v == i:
            return k


def uniq_list_join(joiner, split_plats):
    return str.join(joiner, sorted(set(split_plats)))


def split_strip(splitter:str, str_to_split:str) -> [str]:
    split_str = str_to_split.split(splitter)
    split_str = map(str.strip, split_str)
    split_str = filter(bool, split_str)
    return list(split_str)



def add_row(sheet, new_row):
    sheet.append(new_row)


def read_sheet(input_sheet_name):

    #todo filter out games not played?

    with open(input_sheet_name, newline='', encoding='utf-8') as input_csvfile:
        reader = csv.reader(input_csvfile, delimiter='\t', quotechar='|')

        header = next(reader)
        played_games = []
        not_played_games = []
        for str_row in reader:

            if "y" in str_row[COL_PLAYED]:
                row = str_row
                row[COL_PLUS] = int(str_row[COL_PLUS])
                row[COL_EQ] = int(str_row[COL_EQ])
                row[COL_NEG] = int(str_row[COL_NEG])
                row[COL_SCORE] = int(str_row[COL_SCORE])
                row[COL_CMP_COUNT] = int(str_row[COL_CMP_COUNT])
                played_games.append(row)
            else:
                not_played_games.append(str_row)

        return header, played_games, not_played_games


def write_sheet(output_file_name, header, played_sheet, not_played_sheet):
    with open(output_file_name, "w", encoding='utf-8', newline='') as output_csvfile:
        writer = csv.writer(output_csvfile, delimiter='\t', quotechar='|')
        writer.writerow(header)
        for row in played_sheet:
            writer.writerow(row)
        for row in not_played_sheet:
            writer.writerow(row)


def prompt_input():
    return input("> ").lower().strip()

class GameChooser:

    default_user_filter = "cell"
    filter_prefix = "filter_sheet_by_"

    def __init__(self, sheet):
        """
        :type sheet: list[list[str|int]]
        """
        assert len(sheet) > 2
        self.sheet = sheet
        self.choices = [sheet[0], sheet[1]]

        self.current_sheet_filter_methods = [self.filter_sheet_by_few_ratings, self.filter_sheet_by_few_ratings]

        self.user_filter_row = COL_NAME
        self.user_filter_expr = "True"

        rv = self.set_user_filter("name", 'True')
        assert rv is True

        self.fighter = None
        self.remaining_opponents = []


    def set_sheet_filter_method_by_name(self, i, choice):
        choices = self.get_available_sheet_filters()
        assert choice in choices
        assert 0 <= i <= len(self.current_sheet_filter_methods)

        choice_func_name = next(x for x in choices if x == choice)
        bound_method = self.resolve_filter_function_name(choice_func_name)
        self.current_sheet_filter_methods[i] = bound_method


    def choose_row(self, i):
        filter_method = self.current_sheet_filter_methods[i]
        filtered_sheet = filter_method()
        sheet_len = len(filtered_sheet)
        if sheet_len <= 0:
            print("ERROR: Current filter results in 0 rows", file=sys.stderr)
            return
        row_index = random.randint(0, sheet_len - 1)
        self.choices[i] = filtered_sheet[row_index]

    def choose_two_rows(self):
        self.choose_row(0)
        self.choices[1] = self.choices[0]

        num_attempts = 0
        while self.choices[1] == self.choices[0]:
            self.choose_row(1)
            num_attempts += 1
            if num_attempts > 10:
                print("ERROR: Choice problem??!? Choice B == Choice A?", file=sys.stderr)
                break

        return self.choices

    def change_single_mode(self, choice_i):
        print()
        print("current filter:")
        print("\t" + self.get_current_sheet_filters()[choice_i])

        choice = get_user_operation_enumerated(
            "Available sheet filters",
            self.get_available_sheet_filters())
        if choice is "exit":
            return

        self.set_sheet_filter_method_by_name(choice_i, choice)

    def change_mode(self):
        input_choices = {
            "set_a": ["a", "1"],
            "set_b": ["b", "2"],
            "change_user_filter": ["user_filter", "3", "c", "u"],
            "enter_fight_mode": ["fight_mode", "4", "f"]
        }

        operation = ""
        while operation is not "exit":
            print()
            print("Edit filters for A or B, or edit user filter.")
            print("Current filters:")
            print("\t" + str.join("; ", self.get_current_sheet_filters()))

            operation = get_user_operation(input_choices, key=key_by_ordinal)
            if operation == "set_a":
                self.change_single_mode(0)
            elif operation == "set_b":
                self.change_single_mode(1)
            elif operation == "change_user_filter":
                self.change_user_filter()
            elif operation == "enter_fight_mode":
                ok = self.enter_fight_mode(quick_mode=False)
                if ok:
                    # skip straight back to main menu if
                    # everything went ok
                    return

        return


    def filter_sheet_for_fighting(self, fighter, sheet):
        if fighter in sheet:
            sheet.remove(fighter)

        def find_better(item):
            return fighter[COL_SCORE] <= item[COL_SCORE]

        better_ranked_sheet = filter(find_better, sheet)
        sorted_sheet = sorted(better_ranked_sheet,
                              key=GameChooser.sort_sheet_by_score)

        self.remaining_opponents = sorted_sheet

    def enter_fight_mode(self, quick_mode):
        print("Entering fight mode!")

        # always rescore sheet so nothing shares the same score.
        # avoids skipping more things than intended.
        #rescore_sheet(self.sheet)

        fighter = self.fight_mode_select_champion(quick_mode)
        if fighter is None:
            print("fight mode abandoned")
            return False

        self.fighter = fighter

        sheet = list(self.sheet)
        self.filter_sheet_for_fighting(fighter, sheet)

        self.current_sheet_filter_methods[0] = self.fight_mode_select_fighter
        self.current_sheet_filter_methods[1] = self.fight_mode_select_opponent
        return True


    def search_for_game(self):
        rv = False
        while rv is False:

            print()
            print("Enter string to search game names")
            print("Enter '' to leave alone and exit prompt")
            text = prompt_input()

            if text == "":
                return None

            def find_name(item):
                return text.lower() in item[COL_NAME].lower()
            candidate_games = list(filter(find_name, self.sheet))
            if not candidate_games:
                print("No games found for string '{}'".format(text))
                continue

            print("Found {} games matching string".format(
                len(candidate_games)))

            candidate_games = candidate_games[0:10]
            candidate_dict = {}
            candidate_names = []
            for game in candidate_games:
                name = game[COL_NAME]
                candidate_dict[name] = game
                candidate_names.append(name)

            choice = get_user_operation_enumerated(
                "First {} found games".format(len(candidate_names)),
                candidate_names)
            if choice is "exit":
                continue
            return candidate_dict[choice]

    def resolve_filter_function_name(self, choice_func_name):
        return getattr(self, self.filter_prefix + choice_func_name)

    def fight_mode_select_champion(self, quick_mode):

        if quick_mode:
            few_ratings = self.filter_sheet_by_few_ratings()
            low_score = self.filter_sheet_by_low_score()
            low_sheet = [v for v in few_ratings if v in low_score]

            if not low_sheet:
                low_sheet = few_ratings
            row_index = random.randint(0, len(low_sheet) - 1)
            return low_sheet[row_index]

        print("Choose champion game to fight with")
        print("You can use current A or B game, or search for a game")
        input_choices = {
            "use_current_a": ["a", "1"],
            "use_current_b": ["b", "2"],
            "search_for_game": ["search", "3", "s"],
            "random_from_filter": ["filter", "4", "f"]
        }
        operation = get_user_operation(input_choices, key=key_by_ordinal)
        if operation == "exit":
            return None
        elif operation == "search_for_game":
            return self.search_for_game()
        elif operation == "use_current_a":
            return self.choices[0]
        elif operation == "use_current_b":
            return self.choices[1]
        elif operation == "random_from_filter":
            #choice_func_name = get_user_operation_enumerated(
            #    "Available sheet filters",
            #    sheet_filters)
            #if choice_func_name is "exit":
            #    return None
            #bound_method = self.resolve_filter_function_name(choice_func_name)
            #return bound_method()

            # just overwrite what's in slot B.
            self.change_single_mode(1)
            self.choose_row(1)
            return self.choices[1]
        else:
            print("err")
            return

    def fight_mode_select_fighter(self):
        return [self.fighter]

    def fight_mode_select_opponent(self):
        sheet_len = len(self.remaining_opponents)
        if sheet_len:

            # we want to avoid a game right at the bottom and being
            # compared to one right at the top and losing, even though
            # it might want to go into the top 100 etc
            # we want a better scheme than this. maybe something like:
            # have a max index. You can only get into the:
            #   top 10 if you're in the top 20
            #   top 20 if you're in the top 50
            #   top 50 if you're in the top 100
            #   top 100 if you're in the top 200
            #   top 200 if you're in the top 500
            #   top 500 if you're anywhere
            # etc
            #if sheet_len < 51:
            #    l = [(x[0], x[1]) for x in self.remaining_opponents]
            #    pprint.pprint(list(enumerate(reversed(l))))

            max_index = max(sheet_len // 2, 1)
            row_index = random.randint(0, max_index - 1)
            opp = self.remaining_opponents.pop(row_index)
            return [opp]
        else:
            print("NO OPPONENTS LEFT! Choose from all!")
            return self.sheet

    def is_fight_mode(self):
        return self.current_sheet_filter_methods[1] == self.fight_mode_select_opponent

    def process_operation(self, operation):
        update_fighting_sheet = False
        if operation in ["a_wins", "eq"]:
            update_fighting_sheet = True
        elif operation == "b_wins":
            a,b = self.choices
            msg = "Challenger {} beats current fighter {}! Switching."
            print(msg.format(b[COL_NAME], a[COL_NAME]))

            self.fighter = self.choices[1]
            update_fighting_sheet = True
        else:
            #ignore
            pass

        # fight mode rescores at the appropriate time.
        # Otherwise we can just rescore safely.
        #if not self.is_fight_mode():
        #    rescore_sheet(self.sheet)

        if update_fighting_sheet:
            self.filter_sheet_for_fighting(
                self.fighter,
                self.remaining_opponents)

        if len(self.remaining_opponents) == 0 and self.is_fight_mode():
            print("No opponents left. Restarting fight mode")
            self.enter_fight_mode(quick_mode=True)





    def change_user_filter_col(self):
        input_choices = {}

        print()
        print("Available columns to filter on:")
        for k,v in COLUMN_NAMES.items():
            print("\t" + k)
            input_choices[k] = [k, str(v)]

        operation = get_user_operation(input_choices, key=key_by_ordinal)
        if operation is not "exit":
            rv = self.set_user_filter_row(operation)
            assert rv is True

    def change_user_filter_expr(self):
        rv = False
        while rv is False:
            print()
            print("Enter any python expression operating on variable 'cell'.")
            print("cell will be a string or int: depends on column")
            print("Enter '' to leave alone and exit prompt")
            text = prompt_input()
            if text == "":
                return
            rv = self.set_user_filter_row_expr(text)

    def change_user_filter(self):
        input_choices = {
            "set_col": ["set_col", "1", "c"],
            "set_expr": ["set_expr", "2", "e"],
        }

        operation = ""
        while operation is not "exit":
            print()
            print("Changing user filter")
            print("Current user filter:")
            print("\tcolumn={} ({})".format(self.user_filter_row,
                                            reverse_column_name(self.user_filter_row)))
            print("\texpression:" + repr(self.user_filter_expr))
            operation = get_user_operation(input_choices)
            if operation == "set_col":
                self.change_user_filter_col()
            elif operation == "set_expr":
                self.change_user_filter_expr()
            else:
                assert operation is "exit"

    def set_user_filter_row(self, row):
        row_upper = row.upper()
        if row_upper not in COLUMN_NAMES:
            return False
        self.user_filter_row = COLUMN_NAMES[row_upper]
        return True

    def test_user_filter_expr(self, expr, cell):
        try:
            test = eval(expr)
            assert isinstance(test, bool)
            return True
        except Exception as ex:
            print(ex)
            return False

    def set_user_filter_row_expr(self, expr):

        print("Testing expr against int..")
        rv = self.test_user_filter_expr(expr, 512)
        if not rv:
            print("Testing expr against str(int)...")
            rv = self.test_user_filter_expr(expr, "512")
        if not rv:
            print("Testing expr against str...")
            rv = self.test_user_filter_expr(expr, "Doom II")

        if not rv:
            print("All tests failed. Setting to default" + repr(self.default_user_filter))
            self.user_filter_expr = self.default_user_filter
            return False

        print("Test passed")
        self.user_filter_expr = expr
        return True


    def set_user_filter(self, row, expr):
        if not self.set_user_filter_row(row):
            return False

        if not self.set_user_filter_row_expr(expr):
            return False

        return True


    def get_current_sheet_filters(self):
        return [f.__func__.__name__.replace(self.filter_prefix, "") for f in self.current_sheet_filter_methods]

    def get_available_sheet_filters(self):
        """
        :rtype : [str]
        """
        choices = []
        for func_name in dir(self):
            if func_name.startswith(self.filter_prefix):
                choices.append(func_name.replace(self.filter_prefix, ""))
        return choices

    def filter_sheet_by_from_all(self):
        return self.sheet

    @staticmethod
    def reduce_sheet_range(sorted_sheet):
        i = max(len(sorted_sheet) // 8, 1)
        return sorted_sheet[0:i]

    def filter_sheet_by_many_ratings(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_num_choices, reverse=True)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_few_ratings(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_num_choices)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_high_score(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_score, reverse=True)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_low_score(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_score)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_many_upvotes(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_upvotes, reverse=True)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_many_equals(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_equals, reverse=True)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_many_downvotes(self):
        sorted_sheet = sorted(self.sheet, key=GameChooser.sort_sheet_by_downvotes, reverse=True)
        return self.reduce_sheet_range(sorted_sheet)

    def filter_sheet_by_user_filter(self):

        def cc(row):
            cell = row[self.user_filter_row]
            if isinstance(cell, str):
                cell = cell.lower()
            else:
                assert isinstance(cell, int)
            return eval(self.user_filter_expr)
        return list(filter(cc, self.sheet))

    @staticmethod
    def sort_sheet_by_num_choices(row):
        return row[COL_CMP_COUNT]

    @staticmethod
    def sort_sheet_by_score(row):
        return row[COL_SCORE]

    @staticmethod
    def sort_sheet_by_downvotes(row):
        return row[COL_NEG]

    @staticmethod
    def sort_sheet_by_upvotes(row):
        return row[COL_PLUS]

    @staticmethod
    def sort_sheet_by_equals(row):
        return row[COL_EQ]


def print_games(chooser, more):
    """
    :type more: bool
    :type chooser: GameChooser
    """
    row_a, row_b = chooser.choices
    if more:
        a = row_a
        b = row_b
    else:
        a = row_a[COL_NAME]
        b = row_b[COL_NAME]
    msg = "\t{}:{:>3d}/{:>2d}, '{}'"
    print(msg.format("A", row_a[COL_SCORE], row_a[COL_CMP_COUNT], a))
    print("vs")
    print(msg.format("B", row_b[COL_SCORE], row_b[COL_CMP_COUNT], b))



def op_str(op):
    """
    :rtype : str
    """
    return "/".join(op[0:2])

def get_user_operation(input_choices, key=None):
    """
    Prompt user for input and translate that into an operation
    Possible user input and operation are given in input_choices
    "exit" operation is automatically added to available choices

    :param input_choices: dict: operation token and a list of user inputs that token supports
    :type input_choices: dict[str, list[str]]
    :rtype : str
    """

    exit = ["exit", "x", "quit", "q"]

    op_strs = []
    for possible_inputs in sorted(input_choices.values(), key=key):
        op_strs.append(op_str(possible_inputs))
    op_strs.append(op_str(exit))
    available_ops_str = "; ".join(op_strs)

    while True:
        print()
        print(available_ops_str)
        text = prompt_input()

        if text in exit:
            return "exit"

        for (choice, possible_inputs) in input_choices.items():
            if text in map(str.lower, possible_inputs):
                return choice

        print("unknown input " + repr(text))
        print("---")

def key_by_ordinal(item):
    return int(item[1])

def get_user_operation_enumerated(set_string, choices):
    input_choices = {}
    print(set_string)
    for (i, choice) in enumerate(choices):
        print("\t" + choice)
        input_choices[choice] = [choice, str(i+1)]

    return get_user_operation(input_choices, key=key_by_ordinal)


def process_operation(chooser, operation):
    """
    :type operation: str
    :type chooser: GameChooser
    """
    row_a, row_b = chooser.choices
    if operation == "exit":
        return False
    elif operation == "a_wins":
        row_a[COL_SCORE] = max(row_a[COL_SCORE],
                               row_b[COL_SCORE] + 1)
        row_a[COL_PLUS] += 1
        row_a[COL_CMP_COUNT] += 1

        row_b[COL_NEG] += 1
        row_b[COL_CMP_COUNT] += 1
    elif operation == "ab_equal":
        row_a[COL_SCORE] = max(row_a[COL_SCORE],
                               row_b[COL_SCORE])
        row_a[COL_EQ] += 1
        row_a[COL_CMP_COUNT] += 1

        row_b[COL_SCORE] = max(row_a[COL_SCORE],
                               row_b[COL_SCORE])
        row_b[COL_EQ] += 1
        row_b[COL_CMP_COUNT] += 1
    elif operation == "b_wins":
        row_a[COL_NEG] += 1
        row_a[COL_CMP_COUNT] += 1

        row_b[COL_SCORE] = max(row_a[COL_SCORE] + 1,
                               row_b[COL_SCORE])
        row_b[COL_PLUS] += 1
        row_b[COL_CMP_COUNT] += 1

    elif operation == "dupe":
        if row_a[COL_DUPE] not in ["", "y"]:
            print("replacing a's dupe col" + repr(row_a[COL_DUPE]))
        if row_b[COL_DUPE] not in ["", "y"]:
            print("replacing b's dupe col" + repr(row_b[COL_DUPE]))

        row_a[COL_DUPE] = "y"
        row_a[COL_CMP_COUNT] += 1

        row_b[COL_DUPE] = "y"
        row_b[COL_CMP_COUNT] += 1
    elif operation == "skip":
        # todo: should probably have a COL_NUM_SKIPS? :)
        pass
    elif operation == "change_mode":
        chooser.change_mode()
    elif operation == "enter_fight_mode":
        chooser.enter_fight_mode(quick_mode=True)
    else:
        # more is handled outside this function.
        assert operation != "more"

        print("UNKNOWN COMMAND: " + repr(operation),
              file=sys.stderr)
        return False

    return True


def have_user_rate_games(played_sheet):

    input_choices = {
        "a_wins": ["a", "1", "p"],
        "b_wins": ["b", "2", "n"],
        "eq": ["eq", "=", "e"],
        "dupe": ["dupe", "!", "d"],
        "more": ["more", "?", "m"],
        "skip": ["skip", "s"],
        "change_mode": ["chooser", "c"],
        "enter_fight_mode": ["FIGHT!", "f", "+"]
    }

    chooser = GameChooser(played_sheet)

    chooser.enter_fight_mode(quick_mode=True)

    more = False
    user_is_rating_games = True
    while user_is_rating_games:

        if not more:
            chooser.choose_two_rows()

        print_games(chooser, more)
        operation = get_user_operation(input_choices)
        print(operation)
        more = (operation == "more")
        if not more:
            user_is_rating_games = process_operation(chooser, operation)
            chooser.process_operation(operation)
        print("-----")
        print()

    return


def reset_sheet(played_sheet):
    for (i, row) in enumerate(played_sheet):
        row[COL_CMP_COUNT] = 0
        row[COL_EQ] = 0
        row[COL_PLUS] = 0
        row[COL_NEG] = 0
        row[COL_SCORE] = i

def rescore_sheet(played_sheet):
    played_sheet.sort(key=GameChooser.sort_sheet_by_score)
    for (i, row) in enumerate(played_sheet):
        row[COL_SCORE] = i

def main():
    import argparse

    parser = argparse.ArgumentParser(description='Filter wikipedia duplicated crap from tsv files')
    parser.add_argument('input', metavar='FILENAME', type=str,
                        help='input file', default="test_file.tsv")
    args = parser.parse_args()
    header, played_sheet, not_played_sheet = read_sheet(args.input)

    try:
        have_user_rate_games(played_sheet)
        #reset_sheet(played_sheet)
    finally:
        #rescore_sheet(played_sheet)
        sorted_sheet = list(sorted(played_sheet))
        write_sheet(args.input, header, sorted_sheet, not_played_sheet)


if __name__ == '__main__':
    main()
